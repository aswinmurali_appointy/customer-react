import { useState } from "react"

export interface CustomerState {
    name: string,
    email: string,
    phoneNo: string
}

export const useCustomers = () => {
    const [customers, setCustomers] = useState<CustomerState[]>([]);

    const update = (customer: CustomerState) => {
        if (!customers.includes(customer)) {
            setCustomers([...customers, customer]);
        }
        else {
            let loc = customers.findIndex((value) => value.email === customer.email);
            customers[loc] = customer;
            setCustomers([...customers]);
        }
    }

    const remove = (customer: CustomerState) => {
        setCustomers(customers.filter((value) => value.email !== customer.email));
    }

    const get = (name: string) => {
        return customers.filter((value) => value.name === name)[0];
    }

    return [customers, update, remove, get] as const;
}
