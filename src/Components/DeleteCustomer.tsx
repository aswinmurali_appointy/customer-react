import {
    Alert,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Snackbar
} from "@mui/material";
import { useState } from "react";
import { CustomerState } from "../Models/Customer";

export interface DeleteDialogProps {
    show: boolean,
    onClose: () => void,
    customer: CustomerState,
    removeCustomer: (customer: CustomerState) => void,
}

export default function DeleteCustomerDialog(props: DeleteDialogProps) {
    const { show, customer, onClose, removeCustomer } = props;

    const [openNotification, setNotification] = useState(false);

    const confirm = () => {
        removeCustomer(customer);
        setNotification(true);
        onClose();
    }

    return <>
        <Snackbar open={openNotification} anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center'
        }} onClose={() => setNotification(false)} autoHideDuration={2000} >
            <Alert severity='info' onClose={() => setNotification(false)} >
                Customer deleted successfully
            </Alert>
        </Snackbar>
        <Dialog onClose={onClose} open={show}>
            <DialogTitle>Delete Customer</DialogTitle>
            <DialogContent>
                <DialogContentText id='alert-dialog-description'>
                    Are you sure you want to continue?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose}>CANCEL</Button>
                <Button onClick={confirm} variant='contained'>CONFIRM</Button>
            </DialogActions>
        </Dialog>
    </>
}
