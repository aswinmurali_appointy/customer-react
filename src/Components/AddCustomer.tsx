import {
    Alert,
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton,
    Snackbar,
    TextField
} from "@mui/material";

import { useState } from "react";

import '@mui/icons-material';
import { Close } from "@mui/icons-material";
import { CustomerState } from "../Models/Customer";

interface DialogProps {
    show: boolean,
    onClose: () => void,
    addCustomer: (customer: CustomerState) => void,
}

export function AddCustomerDialog(props: DialogProps) {
    const { show, onClose, addCustomer } = props;

    const [showNotification, setNotification] = useState(false);

    const customer = {} as CustomerState;

    const handleSave = () => {
        addCustomer(customer);
        setNotification(true);
        onClose();
    }

    return <>
        <Snackbar open={showNotification} anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center'
        }} onClose={() => setNotification(false)} autoHideDuration={2000} >
            <Alert severity='success' onClose={() => setNotification(false)} >
                Customer added successfully
            </Alert>
        </Snackbar>
        <Dialog onClose={onClose} open={show} fullWidth={true}>
            <DialogTitle>
                <Box display='flex' alignItems='center'>
                    <Box flexGrow={1}>New Customer</Box>
                    <Box>
                        <IconButton onClick={onClose}><Close /></IconButton>
                    </Box>
                </Box>
            </DialogTitle>
            <DialogContent>
                <TextField
                    fullWidth={true} label='Name' variant='standard'
                    onChange={(value) => customer.name = value.target.value}
                />
            </DialogContent>
            <DialogContent>
                <TextField
                    fullWidth={true} label='Email' variant='standard'
                    onChange={(value) => customer.email = value.target.value}
                />
            </DialogContent>
            <DialogContent>
                <TextField
                    fullWidth={true} label='Phone' variant='standard'
                    onChange={(value) => customer.phoneNo = value.target.value}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose}>CANCEL</Button>
                <Button onClick={handleSave} variant='contained'>SAVE</Button>
            </DialogActions>
        </Dialog>
    </>

}
