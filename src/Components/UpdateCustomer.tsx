import {
    Alert,
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    IconButton,
    Snackbar,
    TextField
} from "@mui/material";

import { useState } from "react";

import '@mui/icons-material';
import { Close } from "@mui/icons-material";
import { CustomerState } from "../Models/Customer";

interface DialogProps {
    show: boolean,
    onClose: () => void,
    customer: CustomerState,
    updateCustomer: (customer: CustomerState) => void,
}

export function ModifyCustomerDialog(props: DialogProps) {
    const { show, onClose, updateCustomer } = props;

    const [showNotification, setNotification] = useState(false);

    let localCustomer = props.customer;

    const handleSave = () => {
        console.log(localCustomer);
        updateCustomer(localCustomer);
        setNotification(true);
        onClose();
    }

    return <>
        <Snackbar open={showNotification} anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center'
        }} onClose={() => setNotification(false)} autoHideDuration={2000} >
            <Alert severity='success' onClose={() => setNotification(false)} >
                Customer added successfully
            </Alert>
        </Snackbar>
        <Dialog onClose={onClose} open={show} fullWidth={true}>
            <DialogTitle>
                <Box display='flex' alignItems='center'>
                    <Box flexGrow={1} >Update Customer</Box>
                    <Box>
                        <IconButton onClick={onClose}>
                            <Close />
                        </IconButton>
                    </Box>
                </Box>
            </DialogTitle>
            <DialogContent>
                <TextField
                    value={localCustomer.name} fullWidth={true} label='Name'
                    variant='standard' onChange={(value) => localCustomer.name = value.target.value}
                />
            </DialogContent>
            <DialogContent>
                <TextField
                    value={localCustomer.email} fullWidth={true} label='Email'
                    variant='standard' onChange={(value) => localCustomer.email = value.target.value}
                />
            </DialogContent>
            {/* <DialogContent>
                <MuiTelInput onChange={() => { }} value={"23232"} />
            </DialogContent> */}
            <DialogContent>
                <TextField
                    value={localCustomer.phoneNo} fullWidth={true} label='Phone'
                    variant='standard' onChange={(value) => localCustomer.phoneNo = value.target.value}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={onClose}>CANCEL</Button>
                <Button onClick={handleSave} variant='contained'>SAVE</Button>
            </DialogActions>
        </Dialog>
    </>

}
