import * as React from 'react';

import InboxIcon from '@mui/icons-material/Inbox';
import DraftsIcon from '@mui/icons-material/Drafts';
import AddIcon from '@mui/icons-material/Add';

import {
  Container,
  Divider,
  Fab,
  Grid,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText
} from '@mui/material';
import DeleteCustomerDialog from './Components/DeleteCustomer';
import { ModifyCustomerDialog } from './Components/UpdateCustomer';
import { CustomerState, useCustomers } from './Models/Customer';
import { useState } from 'react';
import { Delete } from '@mui/icons-material';
import { AddCustomerDialog } from './Components/AddCustomer';

export default function App() {
  const [
    customers, updateCustomer,
    removeCustomer, getCustomer
  ] = useCustomers();

  const [selectedCustomer, setSelectedCustomer] = useState({} as CustomerState);
  const [openDeleteDialog, showDeleteDialog] = useState(false);
  const [openAddDialog, showAddDialog] = useState(false);
  const [openUpdateDialog, showUpdateDialog] = useState(false);

  return <>
    {/* Dialogs */}
    <DeleteCustomerDialog
      removeCustomer={removeCustomer} customer={selectedCustomer}
      show={openDeleteDialog} onClose={() => showDeleteDialog(false)}
    />
    <AddCustomerDialog
      show={openAddDialog} addCustomer={updateCustomer}
      onClose={() => showAddDialog(false)}
    />
    <ModifyCustomerDialog
      updateCustomer={updateCustomer} customer={selectedCustomer}
      show={openUpdateDialog} onClose={() => showUpdateDialog(false)}
    />
    {/* Main Layouts */}
    <Container maxWidth="sm">
      <Grid container spacing={2}>
        <Grid item xs={6}>
          Customers
        </Grid>
        <Grid item xs={6}>
          Search Bar
        </Grid>
        <Grid item xs={12}>
          <Fab color="primary" aria-label="add" onClick={() => showAddDialog(true)}>
            <AddIcon />
          </Fab>
          <nav aria-label="main mailbox folders">
            <List>
              {customers.map(
                (customer) => <ListItem disablePadding>
                  <ListItemButton>
                    <ListItemIcon onClick={() => {
                      setSelectedCustomer(customer);
                      showDeleteDialog(true);
                    }}>
                      <Delete />
                    </ListItemIcon>
                    <ListItemText primary={customer.name}
                      onClick={() => {
                        setSelectedCustomer(customer);
                        showUpdateDialog(true);
                      }}
                    />
                  </ListItemButton>
                </ListItem>
              )}
            </List>
          </nav>
        </Grid>
      </Grid>
    </Container>
  </>
}